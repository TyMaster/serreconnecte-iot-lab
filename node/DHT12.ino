#include <DHT.h>

#define DHTPIN D1     // what digital pin the DHT22 is conected to
#define DHTTYPE DHT22   // there are multiple kinds of DHT sensors
#define Water_Sensor D5 // what digital pin the water  sensor is connected to
#define Lumen_Sensor D3 // what digital pin the photoresistor is connected to
int moisture_pin = A0; 

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(5000);

  // Wait for serial to initialize.
   while(!Serial) { }

  Serial.println("Device Started");
  Serial.println("-------------------------------------");
  Serial.println("Running Sensor !");
  Serial.println("-------------------------------------");

}

int timeSinceLastRead = 0;
void loop() {

  // Report every 2 seconds.
  if(timeSinceLastRead > 1000) {
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    float h = dht.readHumidity();
    // Read temperature as Celsius (the default)
    float t = dht.readTemperature();
    // Read moisture from FC-28
    float moisture_value = analogRead(moisture_pin);
    // Map value from min = 0 to max=550, to 100 for the %
    moisture_value = map(moisture_value,550,0,0,100);
    // Read the rain value (should be analog but not enough pin)
    float rain_value= digitalRead(Water_Sensor);
    // Read the lumen value (should be analog but not enough pin)
    float lumen = digitalRead(Lumen_Sensor);
    // Check if any reads failed and exit early (to try again).
   /* if (isnan(h) || isnan(t)) {
      Serial.println("Failed to read from DHT sensor!");
      timeSinceLastRead = 0;
      return;
    }*/
    
    Serial.print(h);
    Serial.print(",");
    Serial.print(" %\t");
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.println(" *C ");
    Serial.print("Soil Humidity : ");
    Serial.print(moisture_value);
    Serial.println(" %");
    Serial.print("Is raining :");
    Serial.println(rain_value);
    Serial.print("Is daytime : ");
    Serial.println(lumen);
    Serial.println();
    Serial.println();               

    timeSinceLastRead = 0;
  }
  delay(100);
  timeSinceLastRead += 100;
}
